package com.gary.springboot.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value="这是model")

public class GaryModel {

    @ApiModelProperty(value = "年龄",required=true)
    private Integer age;
    @ApiModelProperty(value = "姓名",required=false)
    private String name;
    @ApiModelProperty(value = "性别",required=true)
    private Integer sex;

    public Integer getAge() {
        return age;
    }

    public static void setAge(Integer age) {

    }

    public String getName() {
        return name;
    }

    public static void setName(String name) {

    }

    public Integer getSex() {
        return sex;
    }

    public static void setSex(Integer sex) {

    }
}
