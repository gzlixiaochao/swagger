package com.gary.springboot.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import com.gary.springboot.model.GaryModel;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@Api(tags="用户操作接口")
@RequestMapping("stmt/stmtInfo")
public class gary {
    //访问的路径



    @GetMapping({"list111111"})
    @ApiOperation(value="gary方法1",nickname="gary_nickname_01")
    @ApiImplicitParams({
            @ApiImplicitParam(name="key",value = "页码",required=false,paramType="header",dataType="Long"),
            @ApiImplicitParam(name="page",value = "页码",required=false,paramType="form",dataType="integer"),
            @ApiImplicitParam(name="size",value = "每页记录数",required=true,paramType="form",dataType="integer")
    })
    public String helloGary1(){
        return "hello gary";
    }
//    public GaryModel helloGary1(){
//        GaryModel person = new GaryModel();
//        return person;
//    }


    @GetMapping("list2")
    @ApiOperation(value="gary方法2",notes="这是notes",httpMethod="这是httpMethod",code=200)
    @ApiImplicitParams({
            @ApiImplicitParam(name="page",value = "页码",required=true,paramType="form",dataType="integer"),
            @ApiImplicitParam(name="size",value = "每页记录数",required=true,paramType="form",dataType="integer")
    })
//    public String helloGary2(){
//        return "hello gary";
//    }
    public GaryModel helloGary2(){
        GaryModel person = new GaryModel();
        return person;
    }


}
