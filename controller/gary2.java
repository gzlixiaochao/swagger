package com.gary.springboot.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
@Api(tags="用户操作接口2")
public class gary2 {
    //访问的路径
    @GetMapping("/list3")
    @ApiOperation("gary方法3")
    @ApiImplicitParams({
            @ApiImplicitParam(name="page",value = "页码",required=true,paramType="path",dataType="integer"),
            @ApiImplicitParam(name="size",value = "每页记录数",required=true,paramType="path",dataType="integer")
    })
    public String helloGary3(){
        return "hello gary";
    }
}
